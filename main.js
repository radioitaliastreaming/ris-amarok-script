/*
 * Copyright 2017 Federico Vaga <federico.vaga@vaga.pv.it>
 * License: GPLv3
 */

Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.xml" );
Importer.loadQtBinding( "qt.network" );
Importer.loadQtBinding( "qt.gui" );

var playlist_url = "http://www.radioitaliastreaming.it/radioitaliastreaming.xspf";
var playlist = new QDomDocument();

var RISInfoHTML = "<p>Radio Italia Streaming è una collezione di radio in lingua italiana creato da <a href='mailto:federico.vaga@vaga.pv.it'>Federico Vaga</a>. Siete liberi di contattarmi per segnalare problemi e suggerire modifiche</p>";


/**
 * It gets an XSPF track child by name
 */
function trackAttribute(track, attr_name)
{
    try {
        return track.firstChildElement(attr_name).text();
    } catch (err) {
        return "";
    }
}


/**
 * It sets the information area for a given track
 */
function trackHTML(track)
{
    return "<h1>" + trackAttribute(track, "creator") + "</h1><p><a href='" + trackAttribute(track, "info") + "'>" + trackAttribute(track, "title") + "</a></p>";
}


/**
 * It convers an XSPF track into an Amarok item
 */
function addTrack(track)
{
    track_title = trackAttribute(track, "title");

    Amarok.debug("add track: " + trackAttribute(track, "title") + " " + trackAttribute(track, "creator"));

    item = Amarok.StreamItem;
    item.level = 0;
    item.callbackData = "";
    if (trackAttribute(track, "creator") === "")
        prefix = "";
    else
        prefix = trackAttribute(track, "creator") + " - ";
    item.itemName = prefix + trackAttribute(track, "title");
    item.playableUrl = trackAttribute(track, "location");
    item.infoHtml = trackHTML(track);
    item.coverUrl = trackAttribute(track, "image");

    return item;
}


/**
 * It receives an XSPF playlist. It parses the list and populates Amarok
 */
function savePlaylist(xml)
{
    if (xml) {
        playlist.setContent(xml);

        tracks = playlist.elementsByTagName("track");

        for (i = 0; i < tracks.size(); ++i) {
            item = addTrack(tracks.item(i));
            if (item) {
                script.insertItem(item);
            }
        }
        script.donePopulating();
    }
}


/**
 * It populates Amarok with the list of available radios.
 * It retrieves the radios from www.radioitaliastreaming.it
 */
function onPopulating( level, callbackData, filter )
{
    var url = QUrl.fromEncoded(new QByteArray(playlist_url), QUrl.StrictMode);
    new StringDownloader(url, savePlaylist);
}


/**
 * It customizes the presentation
 */
function onCustomize()
{
    var logo_path = Amarok.Info.scriptPath() + "/radioitaliastreaming.png";
    var logo = new QPixmap(logo_path);
    script.setIcon(logo);
    script.setEmblem(logo);
}


/**
 * It creates the Amarok script service
 */
function RadioItaliaStreaming()
{
    ScriptableServiceScript.call(this, "Radio Italia Streaming", 1,
                                 "Radio in lingua italiana",
				 RISInfoHTML, false);
}

script = new RadioItaliaStreaming();
script.populate.connect(onPopulating);
script.customize.connect(onCustomize);
