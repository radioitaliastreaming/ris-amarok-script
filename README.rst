Amarok Script Radio Italia Streaming
====================================

Attraverso questa estensione per `Amarok`_  è possibile accedere alle
radio di `Radio Italia Streaming`_.

Installazione
-------------

Gestione degli script di Amarok
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Il metodo più semplice, e raccomandato, per installare l'estensione è quello
di utilizzare il servizio di gestion gli script integrato in `Amarok`_.
Andate nella seguente finestra ``Amarok > configura Amarok > Script``, premete
il bottone ``Gestisci gli script``. Ora cercate *Radio Italia Streaming*
e premete ``Installa``.

Catalogo opendesktop
~~~~~~~~~~~~~~~~~~~~
Potete scaricare l'estensione dal catalogo di `OpenDesktop`_ al seguente
indirizzo: https://www.opendesktop.org/p/1135764/ .
Una volta scaricato l'archivio aprite Amarok ed aprite la finestra
``Amarok > configura Amarok > Script``, premete il bottone
``Installa script locale``, quindi selezionate l'archivio precedentemente
scaricato.

Utilizzando i sorgenti
~~~~~~~~~~~~~~~~~~~~~~
Per installare l'estensione dai sorgenti clonate il repositorio git dalla
`pagina del progetto`_ ed utilizzare *make*. Di seguito un esempio.::

    git clone https://gitlab.com/radioitaliastreaming/ris-amarok-script.git
    cd ris-amarok-script
    make install

.. _`OpenDesktop`: https://www.opendesktop.org/
.. _`Amarok`: https://amarok.kde.org/
.. _`Radio Italia Streaming`: http://www.radioitaliastreaming.it/
.. _`pagina del progetto`: https://gitlab.com/radioitaliastreaming/ris-amarok-script
