DEST_DIR = $(HOME)/.kde/share/apps/amarok/scripts/radioitaliastreaming.amarokscript/

PKG=radioitaliastreaming.amarokscript
FILES := main.js script.spec radioitaliastreaming.png
FILES += LICENSE CHANGELOG README.rst

install: uninstall
	mkdir -p $(DEST_DIR)
	install $(FILES) $(DEST_DIR)

uninstall:
	rm -rf $(DEST_DIR)

package:
	rm -f $(PKG).tar.gz
	mkdir -p $(PKG)
	cp $(FILES) $(PKG)
	tar -cf $(PKG).tar.gz $(PKG)
	rm -rf $(PKG)
