[Desktop Entry]
Icon=get-hot-new-stuff-amarok
Type=script
ServiceTypes=KPluginInfo

Name=Radio Italia Streaming
Comment=Radio italiane in streaming

X-KDE-PluginInfo-Author=Federico Vaga
X-KDE-PluginInfo-Email=federico.vaga@vaga.pv.it
X-KDE-PluginInfo-Name=Radio Italia Streaming
X-KDE-PluginInfo-Version=1.0.0
X-KDE-PluginInfo-Category=Scriptable Service
X-KDE-PluginInfo-Depends=Amarok2.1
X-KDE-PluginInfo-License=GPLv3
X-KDE-PluginInfo-EnabledByDefault=true
